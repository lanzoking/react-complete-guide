import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {
  state = {
    persons: [
      {name: 'Max', age: 29},
      {name: 'Manu', age: 29},
      {name: 'Stephanie', age: 26}
    ],
    showPersons: false
  }

  switchNameHandler = (newName) => {
    this.setState({persons: [
      {name: newName, age: 29},
      {name: 'Kadz', age: 24},
      {name: 'Imani', age: 25}
    ]});
  }

  togglePersonsHandler = () => {
    this.setState({showPersons: !this.state.showPersons})
  }

  nameChangeHandler = (event) => {
    this.setState({persons: [
      {name: event.target.value, age: 29},
      {name: 'Kadz', age: 24},
      {name: 'Imani', age: 25}
    ]});
  }

  render() {
    const buttonStyle = {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px', 
      cursor: 'pointer'
    }

    return (
      <div className="App">
        <h1>Lanzo King</h1>

        <button 
          style={buttonStyle}
          onClick={this.togglePersonsHandler}>Show Persons</button>

        {this.state.showPersons &&
          <div>
          <Person 
            name={this.state.persons[0].name} 
            age={this.state.persons[0].age}  
            click={this.switchNameHandler.bind(this,"Lanzo")}
            change={this.nameChangeHandler} />
          <Person 
            name={this.state.persons[1].name} 
            age={this.state.persons[1].age}>Hobby: Coding</Person>
          <Person 
            name={this.state.persons[2].name} 
            age={this.state.persons[2 ].age}/>
        </div>
        }
      </div>
    );
  }
}

export default App;
